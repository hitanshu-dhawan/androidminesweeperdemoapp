package com.example.hitanshu.minesweeper;

import android.content.Context;

/**
 * Created by hitanshu on 17/6/17.
 */

public class App {

    private static Context context;

    public static void setContext(Context context) {
        App.context = context;
    }

    public static Context getContext() {
        return context;
    }
}
