package com.example.hitanshu.minesweeper;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

/**
 * Created by hitanshu on 17/6/17.
 */

public class MyButtonListner implements View.OnClickListener {

    @Override
    public void onClick(View v) {

        MyButton button = (MyButton) v;

        if(!MainActivity.gameStarted) {
            MainActivity.gameStarted = true;
            MainActivity.setUpBombs(button);
            button.isShown = true;
            if(button.number == 0) {
                Cordinate cordinate = MainActivity.getCordinates(button);
                MainActivity.recurZeros(cordinate);
            }
            else {
                button.setText(button.number + "");
                //MainActivity.cellShown++;
                //Log.i("cellShown: ",MainActivity.cellShown+"");
                button.setEnabled(false);
            }
            return;
        }

        if(button.isBomb) {
            //button.isShown = true;
            button.setText("!B!");
            button.setEnabled(false);
            MainActivity.gameOver = true;
            MainActivity.gameEnd();
            Toast.makeText(App.getContext(),"GAME OVER",Toast.LENGTH_SHORT).show();
        }
        else {
            if(button.number == 0) {
                Cordinate cordinate = MainActivity.getCordinates(button);
                //Log.i("loging",cordinate.row + "  " + cordinate.col);
                MainActivity.recurZeros(cordinate);
            }
            else {
                button.isShown = true;
                button.setText(button.number + "");
                //MainActivity.cellShown++;
                //Log.i("cellShown: ", MainActivity.cellShown + "");
                button.setEnabled(false);
            }

        }

        if(MainActivity.hasPlayerWon()) {
            Toast.makeText(App.getContext(),"YOU WON :D",Toast.LENGTH_LONG).show();
            //MainActivity.gameEnd();
        }

    }

}
