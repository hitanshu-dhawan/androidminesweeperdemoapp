package com.example.hitanshu.minesweeper;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by hitanshu on 17/6/17.
 */

public class MyButtonLongListner implements View.OnLongClickListener {

    @Override
    public boolean onLongClick(View v) {
        //Toast.makeText(App.getContext(),"done",Toast.LENGTH_SHORT).show();
        MyButton button = (MyButton) v;

        if(!button.flaged) {
            if(MainActivity.flagsPined == MainActivity.flags) return true;
            button.flaged = !button.flaged;
            button.setTextColor(Color.RED);
            button.setText("F");
            MainActivity.flagsPined++;
            MainActivity.textView.setText("Flags:" + (MainActivity.flags - MainActivity.flagsPined));
        }
        else {
            button.flaged = !button.flaged;
            button.setTextColor(Color.GRAY);
            button.setText("");
            MainActivity.flagsPined--;
            MainActivity.textView.setText("Flags:" + (MainActivity.flags - MainActivity.flagsPined));
        }

        if(MainActivity.hasPlayerWon()) {
            Toast.makeText(App.getContext(),"YOU WON :D",Toast.LENGTH_LONG).show();
            //MainActivity.gameEnd();
        }

        return true;
    }
}
