package com.example.hitanshu.minesweeper;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static TextView textView;
    private LinearLayout mainLayout;
    public static final int rows = 10;
    public static final int columns = 7;
    public static final int bombs = 15;
    public static int flags = 0;
    //public static int cellShown=0;
    public static int flagsPined=0;
    public static LinearLayout linearLayouts[];
    public static MyButton buttons[][];
    public static boolean gameStarted;
    public static boolean gameOver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        App.setContext(MainActivity.this);
        textView = (TextView)findViewById(R.id.flagTextView);
        mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        gameStarted = false;
        gameOver = false;
        setUpBoard();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newGame:
                //Toast.makeText(App.getContext(),"ABHI ISKI CODING KARNI HEIN",Toast.LENGTH_SHORT).show();
                setUpBoard();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setUpBoard() {

        flags = 0;
        flagsPined = 0;
        //cellShown = 0;
        gameStarted = false;
        gameOver = false;
        mainLayout.removeAllViews();
        textView.setText("");

        linearLayouts = new LinearLayout[rows];
        buttons = new MyButton[rows][columns];

        for(int row=0;row < rows;row++) {
            linearLayouts[row] = new LinearLayout(MainActivity.this);
            linearLayouts[row].setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0,1);
            mainLayout.addView(linearLayouts[row],params);
        }

        for(int row=0;row < rows;row++) {
            for(int col=0;col < columns;col++) {
                buttons[row][col] = new MyButton(MainActivity.this);
                buttons[row][col].setTextSize(20);
                buttons[row][col].setTextColor(Color.GRAY);
                buttons[row][col].isBomb = false;
                buttons[row][col].isShown = false;
                buttons[row][col].number = 0;
                buttons[row][col].flaged = false;
                buttons[row][col].setOnClickListener(new MyButtonListner());
                buttons[row][col].setOnLongClickListener(new MyButtonLongListner());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,ViewGroup.LayoutParams.MATCH_PARENT,1);
                linearLayouts[row].addView(buttons[row][col],params);
            }
        }
    }

    public static void setUpBombs(MyButton button) {

        for(int i=0;i<bombs;i++) {
            int range = rows*columns;
            int rand = (int)(Math.random() * range);
            Log.i("SetUpBombs",rand+"");
            int row = rand / columns;
            int col = rand % columns;
            if(buttons[row][col] != button && !buttons[row][col].isBomb) {
                buttons[row][col].isBomb = true;
                flags++;
            }
        }

        for(int row=0;row < rows;row++) {
            for(int col=0;col < columns;col++) {
                if(buttons[row][col] == button) {
                    buttons[row][col].isBomb = false;
                    buttons[row][col].isShown = true;
                    buttons[row][col].number = getCount(row,col);
                }
                else {
                    buttons[row][col].isShown = false;
                    if(!buttons[row][col].isBomb)
                        buttons[row][col].number = getCount(row,col);
                }
            }
        }

        textView.setText("Flags:" + flags);
    }

    public static int getCount(int row, int col) {
        int count=0;
        int arr[] = {-1,0,+1};
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                if(arr[i]==0 && arr[j]==0) continue;
                if(row+arr[i] < 0 || row+arr[i] >= rows) continue;
                if(col+arr[j] < 0 || col+arr[j] >= columns) continue;

                if(buttons[row+arr[i]][col+arr[j]].isBomb) {
                    count++;
                }
            }
        }
        return count;
    }

    public static Cordinate getCordinates(Button button) {
        Cordinate cordinate = new Cordinate();
        for(int i=0;i<rows;i++) {
            for(int j=0;j<columns;j++) {
                if(buttons[i][j] == button) {
                    cordinate.row = i;
                    cordinate.col = j;
                    break;
                }
            }
        }
        return cordinate;
    }

    // recursive function for 0 mines.

    public static void recurZerosHelper(int row,int col,boolean visited[][]) {

        //cellShown++;
        if(buttons[row][col].number != 0) {
            visited[row][col] = true;
            buttons[row][col].setEnabled(false);
            buttons[row][col].isShown = true;
            buttons[row][col].setText(buttons[row][col].number+"");

            //Log.i("cellShown: ",cellShown+"");
            return;
        }

        int arr[] = {-1,0,+1};
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                if(arr[i]==0 && arr[j]==0) continue;
                if(row+arr[i] < 0 || row+arr[i] >= rows) continue;
                if(col+arr[j] < 0 || col+arr[j] >= columns) continue;

                visited[row][col] = true;
                buttons[row][col].setEnabled(false);
                buttons[row][col].isShown = true;
                buttons[row][col].setText("0");
                //cellShown++;
                //Log.i("cellShown: ","row: "+row+" col: "+col+" "+cellShown+"");
                if(!visited[row+arr[i]][col+arr[j]])
                    recurZerosHelper(row+arr[i],col+arr[j],visited);
            }
        }
    }

    public static void recurZeros(Cordinate cordinate) {
        boolean visited[][] = new boolean[rows][columns];
        recurZerosHelper(cordinate.row,cordinate.col,visited);
    }

    public static void gameEnd() {
        //Toast.makeText(App.getContext(),"GAME OVER",Toast.LENGTH_SHORT).show();
        for(int i=0;i<rows;i++) {
            for(int j=0;j<columns;j++) {
                buttons[i][j].isShown = true;
                if(buttons[i][j].isBomb)
                    buttons[i][j].setText("!B!");
                else
                    buttons[i][j].setText(buttons[i][j].number+"");

                buttons[i][j].setEnabled(false);
            }
        }
    }

    public static int cellsShown() {
        int count=0;
        for(int i=0;i<rows;i++) {
            for(int j=0;j<columns;j++) {
                if(buttons[i][j].isShown)
                    count++;
            }
        }
        return count;
    }

    public static boolean hasPlayerWon() {
        if(MainActivity.flagsPined == MainActivity.flags && MainActivity.cellsShown()+MainActivity.flagsPined == MainActivity.rows*MainActivity.columns) {
            //Toast.makeText(App.getContext(),"YOU WON :D",Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

}
